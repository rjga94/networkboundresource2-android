package rjga.android.example.networkboundresource.data

import android.util.Log
import androidx.room.PrimaryKey
import okhttp3.ResponseBody
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject

private const val TAG = "Repo"

data class Repo(
    @PrimaryKey(autoGenerate = false)
    val id: Int,
    val name: String,
    val fullName: String,
    val description: String,
    val login: String,
    val url: String?,
    val starts: Int
) {
    companion object {
        fun parseResponseBody(responseBody: ResponseBody): List<Repo> {
            val list = mutableListOf<Repo>()

            val jsonArray = JSONArray(responseBody.string())
            (0 until jsonArray.length()).forEach {
                try {
                    val obj = jsonArray.getJSONObject(it)

                    val id = obj.getInt("id")
                    val name = obj.getString("name")
                    val fullName = obj.getString("full_name")
                    val description = obj.getString("description")

                    val ownerObj = obj.getJSONObject("owner")
                    val login = ownerObj.getString("login")
                    val url = ownerObj.getString("url")

                    val stars = obj.getInt("stargazers_count")

                    list.add(Repo(id, name, fullName, description, login, url, stars))

                } catch (e: JSONException) {
                    Log.e(TAG, "parseResponseBody: Failed to parse one of the objects in the array")
                }
            }

            return list
        }

        fun parseJson(obj: JSONObject): Repo {
            val id = obj.getInt("id")
            val name = obj.getString("name")
            val fullName = obj.getString("fullName")
            val description = obj.getString("description")
            val login = obj.getString("login")
            val url = obj.getString("url")
            val starts = obj.getInt("starts")

            return Repo(id, name, fullName, description, login, url, starts)
        }
    }

    fun toJson() = JSONObject().apply {
        put("id", id)
        put("name", name)
        put("fullName", name)
        put("description", description)
        put("login", login)
        put("url", url)
        put("starts", starts)
    }
}