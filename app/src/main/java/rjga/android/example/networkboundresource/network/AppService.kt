package rjga.android.example.networkboundresource.network

import android.util.Log
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.Response
import retrofit2.Retrofit
import rjga.android.networkboundresource.LiveDataCallAdapterFactory
import java.util.concurrent.TimeUnit

private const val TAG = "AppService"

class  AppService {

    private val okHttpClient by lazy {
        OkHttpClient.Builder().apply {
            connectTimeout(60, TimeUnit.SECONDS)
            readTimeout(60, TimeUnit.SECONDS)
            writeTimeout(60, TimeUnit.SECONDS)
            addInterceptor(LogInterceptor())
        }.build()
    }

    private val retrofit by lazy {
        Retrofit.Builder().apply {
            baseUrl("https://api.github.com/")
            addCallAdapterFactory(LiveDataCallAdapterFactory())
            client(okHttpClient)
        }.build()
    }

    val api: AppApi = retrofit.create(AppApi::class.java)

    private class LogInterceptor: Interceptor {
        override fun intercept(chain: Interceptor.Chain): Response {
            Log.i(TAG, "Sending request: ${chain.request().url()}")
            Log.i(TAG, "Body: ${chain.request().body()}")
            Log.i(TAG, "headers: ${chain.request().headers()}")
            return chain.proceed(chain.request())
        }
    }

    companion object {
        @Volatile private var instance: AppService? = null
        fun getInstance() = instance ?: synchronized(this) {
            instance ?: AppService().also { instance = it }
        }
    }
}