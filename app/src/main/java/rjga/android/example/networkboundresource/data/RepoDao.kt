package rjga.android.example.networkboundresource.data

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query

@Dao
interface RepoDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun save(repoPage: RepoPage)

    @Query("SELECT * FROM table_repo_page WHERE page = :page")
    fun load(page: Int): LiveData<RepoPage>
}