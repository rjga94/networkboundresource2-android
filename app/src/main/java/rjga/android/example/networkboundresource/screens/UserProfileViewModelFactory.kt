package rjga.android.example.networkboundresource.screens

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import rjga.android.example.networkboundresource.data.RepoRepository

@Suppress("UNCHECKED_CAST")
class UserProfileViewModelFactory(
    private val args: UserProfileFragmentArgs,
    private val repoRepository: RepoRepository
): ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return UserProfileViewModel(
            args,
            repoRepository
        ) as T
    }
}