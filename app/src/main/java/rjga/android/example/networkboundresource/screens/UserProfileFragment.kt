package rjga.android.example.networkboundresource.screens

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.fragment_user_profile.*
import rjga.android.example.networkboundresource.R
import rjga.android.example.networkboundresource.adapters.ReposListAdapter
import rjga.android.example.networkboundresource.base.BaseFragment
import rjga.android.example.networkboundresource.utilities.InjectorUtils
import rjga.android.networkboundresource.Status

class UserProfileFragment : BaseFragment() {

    private val viewModel by lazy {
        val args: UserProfileFragmentArgs by navArgs()
        ViewModelProvider(this, InjectorUtils.provideUserProfileViewModelFactory(requireContext(), args)).get(UserProfileViewModel::class.java)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_user_profile, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val listAdapter = ReposListAdapter()
        recyclerView.apply {
            adapter = listAdapter
            layoutManager = LinearLayoutManager(requireContext())
            addOnScrollListener(object : RecyclerView.OnScrollListener() {
                override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                    val layoutManager = recyclerView.layoutManager as LinearLayoutManager
                    val lastPosition = layoutManager.findLastVisibleItemPosition()
                    if (lastPosition == listAdapter.itemCount - 1) {
                        viewModel.triggerLoadNextPage()
                    }
                }
            })
        }

        buttonTrigger.setOnClickListener {
            viewModel.triggerLoadNextPage()
        }

        viewModel.repos.observe(viewLifecycleOwner, Observer {
            // update UI
            when (it.status) {
                Status.LOADING -> {
                    // show dialog
                    Log.d(TAG, "repos loading...")
                }
                Status.SUCCESS -> {
                    // hide dialog and show data
                    Log.d(TAG, "loaded repos successfully. page = ${it.data?.page}")
                    listAdapter.addData(it.data?.items ?: emptyList())
                }
                Status.ERROR -> {
                    // show error message
                    Log.e(TAG, "error loading repos: ${it.message}")
                }
            }
        })
    }
}
