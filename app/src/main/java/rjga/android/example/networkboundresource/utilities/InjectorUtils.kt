package rjga.android.example.networkboundresource.utilities

import android.content.Context
import rjga.android.example.networkboundresource.data.AppDatabase
import rjga.android.example.networkboundresource.data.RepoRepository
import rjga.android.example.networkboundresource.network.AppService
import rjga.android.example.networkboundresource.screens.UserProfileFragmentArgs
import rjga.android.example.networkboundresource.screens.UserProfileViewModelFactory

object InjectorUtils {
    fun provideUserProfileViewModelFactory(context: Context, args: UserProfileFragmentArgs) = UserProfileViewModelFactory(
        args,
        getRepoRepository(context)
    )

    private fun getRepoRepository(context: Context) = RepoRepository.getInstance(
        getAppService(context),
        AppDatabase.getInstance(context).repoDao()
    )

    private fun getAppService(context: Context) = AppService.getInstance()
}