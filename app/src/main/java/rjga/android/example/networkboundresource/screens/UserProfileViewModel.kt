package rjga.android.example.networkboundresource.screens

import androidx.lifecycle.ViewModel
import rjga.android.example.networkboundresource.data.RepoRepository

class UserProfileViewModel(
    private val args: UserProfileFragmentArgs,
    private val repoRepository: RepoRepository
): ViewModel() {
    val userUid: String = args.userUid
    val repos = repoRepository.getReposPaged()

    fun triggerLoadNextPage() {
        repoRepository.triggerLoadNextPage()
    }
}