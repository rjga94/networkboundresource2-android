package rjga.android.example.networkboundresource.data

import androidx.room.Entity
import androidx.room.PrimaryKey
import rjga.android.example.networkboundresource.data.Repo

@Entity(tableName = "table_repo_page")
class RepoPage(
    @PrimaryKey
    val page: Int,
    val items: List<Repo>
)