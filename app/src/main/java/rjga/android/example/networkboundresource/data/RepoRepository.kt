package rjga.android.example.networkboundresource.data

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import okhttp3.ResponseBody
import rjga.android.example.networkboundresource.network.AppService
import rjga.android.networkboundresource.ApiResponse
import rjga.android.networkboundresource.ApiSuccessResponse
import rjga.android.networkboundresource.PagedNetworkBoundResource

class RepoRepository(
    private val appService: AppService,
    private val repoDao: RepoDao
) {

    private val nextPageTrigger = MutableLiveData<Boolean>()

    fun triggerLoadNextPage() {
        nextPageTrigger.postValue(true)
    }

    fun getReposPaged() = object : PagedNetworkBoundResource<RepoPage>() {

        override fun loadNextPageTrigger(): LiveData<Boolean>? {
            return nextPageTrigger
        }

        override fun loadFromDb(page: Int): LiveData<RepoPage> {
            return repoDao.load(page)
        }

        override fun shouldFetch(data: RepoPage?): Boolean {
            return data == null || data.items.isEmpty()
        }

        override fun createCall(page: Int): LiveData<ApiResponse<ResponseBody>> {
            return appService.api.getRepos("google", page)
        }

        override fun processResponse(response: ApiSuccessResponse<ResponseBody>): RepoPage {
            return RepoPage(response.currentPage, Repo.parseResponseBody(response.body))
        }

        override fun saveCallResult(item: RepoPage) {
            repoDao.save(item)
        }

    }.asLiveData()

    companion object {

        // For Singleton instantiation
        @Volatile private var instance: RepoRepository? = null

        fun getInstance(
            appService: AppService,
            repoDao: RepoDao
        ) =
            instance ?: synchronized(this) {
                instance ?: RepoRepository(
                    appService,
                    repoDao
                ).also { instance = it }
            }
    }
}