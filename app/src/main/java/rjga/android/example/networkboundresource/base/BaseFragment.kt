package rjga.android.example.networkboundresource.base

import androidx.fragment.app.Fragment

abstract class BaseFragment: Fragment() {
    protected val TAG = this::class.java.simpleName
}