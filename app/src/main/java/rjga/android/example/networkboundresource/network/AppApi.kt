package rjga.android.example.networkboundresource.network

import androidx.lifecycle.LiveData
import okhttp3.ResponseBody
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query
import rjga.android.networkboundresource.ApiResponse

interface AppApi {

    @GET("users/{user}/repos")
    fun getRepos(@Path("user") user: String): LiveData<ApiResponse<ResponseBody>>

    @GET("users/{user}/repos")
    fun getRepos(@Path("user") user: String, @Query("page") page: Int): LiveData<ApiResponse<ResponseBody>>
}