package rjga.android.networkboundresource

import androidx.annotation.MainThread
import androidx.annotation.WorkerThread
import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.Dispatchers.Main
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import okhttp3.ResponseBody

abstract class PagedNetworkBoundResource<ResultType>
@MainThread constructor() {

    private val result = MediatorLiveData<Resource<ResultType>>()
    private var page = 1
    private var loading = false

    init {
        getData()

        loadNextPageTrigger()?.observeForever {
            getData()
        }
    }

    private fun getData() {
        if (loading) return else loading = true
        result.value = Resource.loading(null)
        @Suppress("LeakingThis")
        val dbSource = loadFromDb(page)
        result.addSource(dbSource) { data ->
            result.removeSource(dbSource)
            if (shouldFetch(data)) {
                fetchFromNetwork(dbSource)
            } else {
                page += 1
                result.addSource(dbSource) { newData ->
                    result.removeSource(dbSource)
                    setValue(Resource.success(newData))
                    loading = false
                }
            }
        }
    }

    @MainThread
    private fun setValue(newValue: Resource<ResultType>) {
        if (result.value != newValue) {
            result.value = newValue
        }
    }

    private fun fetchFromNetwork(dbSource: LiveData<ResultType>) {
        val apiResponse = createCall(page)
        // we re-attach dbSource as a new source, it will dispatch its latest value quickly
        result.addSource(dbSource) { newData ->
            setValue(Resource.loading(newData))
        }
        result.addSource(apiResponse) { response ->
            result.removeSource(apiResponse)
            result.removeSource(dbSource)
            when (response) {
                is ApiSuccessResponse -> {
                    CoroutineScope(Main).launch {
                        withContext(IO) {
                            saveCallResult(processResponse(response))
                        }
                        val liveData = loadFromDb(page)
                        page = if (response.nextPage != null) response.nextPage!! else page + 1
                        result.addSource(liveData) { newData ->
                            result.removeSource(liveData)
                            setValue(Resource.success(newData))
                            loading = false
                        }
                    }
                }
                is ApiEmptyResponse -> {
                    CoroutineScope(Main).launch {
                        // reload from disk whatever we had
                        val liveData = loadFromDb(page)
                        result.addSource(liveData) { newData ->
                            result.removeSource(liveData)
                            setValue(Resource.success(newData))
                            loading = false
                        }
                    }
                }
                is ApiErrorResponse -> {
                    onFetchFailed()
                    result.addSource(dbSource) { newData ->
                        result.removeSource(dbSource)
                        setValue(Resource.error(response.errorMessage, newData))
                        loading = false
                    }
                }
            }
        }
    }

    protected open fun onFetchFailed() {}

    fun asLiveData() = result as LiveData<Resource<ResultType>>

    @MainThread
    protected open fun loadNextPageTrigger(): LiveData<Boolean>? = null

    @MainThread
    protected abstract fun loadFromDb(page: Int): LiveData<ResultType>

    @MainThread
    protected abstract fun shouldFetch(data: ResultType?): Boolean

    @MainThread
    protected abstract fun createCall(page: Int): LiveData<ApiResponse<ResponseBody>>

    @WorkerThread
    protected abstract fun processResponse(response: ApiSuccessResponse<ResponseBody>): ResultType

    @WorkerThread
    protected abstract fun saveCallResult(item: ResultType)
}