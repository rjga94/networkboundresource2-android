package rjga.android.networkboundresource

enum class Status {
    SUCCESS,
    ERROR,
    LOADING
}